cmake_minimum_required(VERSION 3.15)

project(
  telemetryMonitor
  VERSION 1.0
  LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS OFF)

# find the paho library
find_package(PahoMqttCpp REQUIRED)

# set sources
add_executable(${PROJECT_NAME} telemetryMonitor.cpp serialport.cpp)

# add include paths
target_include_directories(
  ${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
                         ${PahoMqttCpp_DIR}/../../../include)
# link with required libraries
target_link_libraries(${PROJECT_NAME} PRIVATE paho-mqttpp3)

# copy required files into build
file(COPY_FILE ${CMAKE_CURRENT_SOURCE_DIR}/Dockerfile
     ${CMAKE_CURRENT_BINARY_DIR}/Dockerfile)
file(COPY_FILE ${CMAKE_CURRENT_SOURCE_DIR}/Dockerfile_cc
     ${CMAKE_CURRENT_BINARY_DIR}/Dockerfile_cc)
file(COPY_FILE ${CMAKE_CURRENT_SOURCE_DIR}/prepareDockerImage.sh
     ${CMAKE_CURRENT_BINARY_DIR}/prepareDockerImage.sh)
file(COPY_FILE ${CMAKE_CURRENT_SOURCE_DIR}/prepareDockerImage_cc.sh
     ${CMAKE_CURRENT_BINARY_DIR}/prepareDockerImage_cc.sh)
file(COPY_FILE ${CMAKE_CURRENT_SOURCE_DIR}/pushDockerImage.sh
     ${CMAKE_CURRENT_BINARY_DIR}/pushDockerImage.sh)
