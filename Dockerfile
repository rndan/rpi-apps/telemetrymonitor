FROM ubuntu

RUN apt update && apt install libssl-dev -y

ENV LD_LIBRARY_PATH /libs

COPY install.paho.mqtt.cpp/usr/local/lib/libpaho-mqttpp3.so.1 /libs/
COPY install.paho.mqtt.cpp/usr/local/lib/libpaho-mqtt3as.so.1 /libs/
COPY build.telemetryMonitor/telemetryMonitor /mybin/

ENTRYPOINT [ "/mybin/telemetryMonitor" ]
CMD [ "mqtt://172.16.200.20:1883" ]

