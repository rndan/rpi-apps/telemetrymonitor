#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mqtt/async_client.h>
#include <serialport.h>
#include <sstream>
#include <string>

/**
 * Convert NMEA absolute position to decimal degrees
 * "ddmm.mmmm" or "dddmm.mmmm" really is D+M/60,
 * then negated if quadrant is 'W' or 'S'
 */
double gpsToDecimalDegrees(const char* nmeaPos, char quadrant)
{
    double v = 0;
    if(strlen(nmeaPos) > 5)
    {
        char integerPart[3 + 1];
        int digitCount = (nmeaPos[4] == '.' ? 2 : 3);
        memcpy(integerPart, nmeaPos, digitCount);
        integerPart[digitCount] = 0;
        nmeaPos += digitCount;
        v = atoi(integerPart) + atof(nmeaPos) / 60.;
        if(quadrant == 'W' || quadrant == 'S')
            v = -v;
    }
    return v;
}

int main(int argc, char** argv)
{
    using namespace std::chrono_literals;

    if(argc != 2)
    {
        std::cout << "Error: missing mqtt broker address" << std::endl;
        return -1;
    }

    const std::string mqttBrokerAddr{argv[1]};
    const std::string myId{"rpi1"}; //TODO fill from env variable
    const std::string mqttTopic{"rpi-app/telemetry-monitor/position"};
    const int mqttQos{1};

    std::cout << "creating client" << std::endl;
    mqtt::async_client client(mqttBrokerAddr, "");
    std::cout << "connecting to broker" << std::endl;
    client.connect()->wait();
    mqtt::topic topic(client, mqttTopic, mqttQos);

    std::cout << "opening serial device" << std::endl;
    SerialPort serialPort("/dev/ttyACM0", 115200, 1);
    std::cout << "opened serial device" << std::endl;
    double lat{0.0};
    double lon{0.0};
    double speed{0.0};

    while(true)
    {
        std::string serialLine{};
        // read line from serial port
        if(serialPort.readLine(serialLine) > 0)
        {
            if(serialLine.find("$GPRMC") != std::string::npos)
            {
                std::stringstream ss(serialLine);
                std::vector<std::string> split;
                std::string temp;
                while(std::getline(ss, temp, ','))
                {
                    split.push_back(temp);
                }
                if((split.size() == 13) && (split.at(3).length() > 2) && (split.at(5).length() > 2))
                {
                    lat = gpsToDecimalDegrees(split.at(3).c_str(), split.at(4).c_str()[0]);
                    lon = gpsToDecimalDegrees(split.at(5).c_str(), split.at(6).c_str()[0]);
                }
                else
                {
                    lat = 0.0;
                    lon = 0.0;
                }
            }
            if(serialLine.find("$GPVTG") != std::string::npos)
            {
                std::stringstream ss(serialLine);
                std::vector<std::string> split;
                std::string temp;
                while(std::getline(ss, temp, ','))
                {
                    split.push_back(temp);
                }
                if((split.size() == 10) && (split.at(7).length() > 2) && (lat != 0.0) && (lon != 0.0))
                {
                    speed = stod(split.at(7));
                    std::stringstream output;
                    output << std::setprecision(10) << "lat,lon: " << lat << "," << lon << " speed: " << speed << "km/h";
                    std::cout << output.str() << std::endl;
                    topic.publish(myId + " " + output.str())->wait();
                }
            }
        }
    }

    client.disconnect()->wait();

    return 0;
}
