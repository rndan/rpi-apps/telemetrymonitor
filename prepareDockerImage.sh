#!/bin/bash

# copy Dockerfile one dir up
cp -a ./Dockerfile ../
# go one directory up
cd ..

# build image
docker build -t registry.gitlab.com/rndan/rpi-apps/telemetrymonitor:latest .

# cleanup
rm Dockerfile
# go back to original directory
cd -