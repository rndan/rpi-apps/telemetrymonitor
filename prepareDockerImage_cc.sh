#!/bin/bash

# copy Dockerfile one dir up, use the cc (cross compile) version
cp -a ./Dockerfile_cc ../Dockerfile
# go one directory up
cd ..

# copy required libraries from rootfs
mkdir temp_libs
cp -L /opt/pi/usr/local/lib/libpaho-mqttpp3.so.1 ./temp_libs
cp -L /opt/pi/usr/local/lib/libpaho-mqtt3as.so.1 ./temp_libs
cp -L /opt/pi/usr/lib/aarch64-linux-gnu/libssl.so.3 ./temp_libs
cp -L /opt/pi/usr/lib/aarch64-linux-gnu/libcrypto.so.3 ./temp_libs

# build image
docker build -t registry.gitlab.com/rndan/rpi-apps/telemetrymonitor:latest --platform=linux/arm64 .

# cleanup
rm Dockerfile
rm -rf temp_libs
# go back to original directory
cd -